```
GetInboxTasksIntent what's in my inbox
GetInboxTasksIntent what is in my inbox
GetInboxTasksIntent what tasks are in my inbox
GetInboxTasksIntent what must i do next
GetInboxTasksIntent what should i do next
GetInboxTasksIntent what i must do next
GetInboxTasksIntent what i should do next
GetInboxTasksIntent what's next to do
GetInboxTasksIntent what is next to do
GetInboxTasksIntent what are my tasks
GetInboxTasksIntent what tasks do i have
GetInboxTasksIntent what tasks must i do
GetInboxTasksIntent what tasks should i do
```

```
GetDueTodayTasksIntent what tasks are due
GetDueTodayTasksIntent what tasks are due today
GetDueTodayTasksIntent what tasks are overdue
GetDueTodayTasksIntent what tasks are outstanding
GetDueTodayTasksIntent what's due
GetDueTodayTasksIntent what's due today
GetDueTodayTasksIntent what's overdue
GetDueTodayTasksIntent what's outstanding
GetDueTodayTasksIntent what is due
GetDueTodayTasksIntent what is due today
GetDueTodayTasksIntent what is overdue
GetDueTodayTasksIntent what is outstanding
GetDueTodayTasksIntent what must i do today
GetDueTodayTasksIntent what should i do today
GetDueTodayTasksIntent what i must do today
GetDueTodayTasksIntent what i should do today
GetDueTodayTasksIntent what tasks must i do today
GetDueTodayTasksIntent what tasks should i do today
GetDueTodayTasksIntent what's in my today list
GetDueTodayTasksIntent what is in my today list
GetDueTodayTasksIntent what tasks are in my today list
GetDueTodayTasksIntent what's listed today
GetDueTodayTasksIntent what is listed today
GetDueTodayTasksIntent what tasks are listed today
GetDueTodayTasksIntent what tasks are listed for today
```

```
GetDueThisWeekTasksIntent what tasks are due this week
GetDueThisWeekTasksIntent what's due this week
GetDueThisWeekTasksIntent what is due this week
GetDueThisWeekTasksIntent what must i do this week
GetDueThisWeekTasksIntent what should i do this week
GetDueThisWeekTasksIntent what i must do this week
GetDueThisWeekTasksIntent what i should do this week
GetDueThisWeekTasksIntent what is listed this week
GetDueThisWeekTasksIntent what tasks are listed this week
GetDueThisWeekTasksIntent what tasks are listed for this week
```

```
GetStarredTasksIntent what tasks are starred
GetStarredTasksIntent what items are starred
GetStarredTasksIntent what's starred
GetStarredTasksIntent what is starred
GetStarredTasksIntent what's in the starred list
GetStarredTasksIntent what is in the starred list
GetStarredTasksIntent what's in my starred list
GetStarredTasksIntent what is in my starred list
GetStarredTasksIntent what tasks have i starred
GetStarredTasksIntent what items have i starred
GetStarredTasksIntent what tasks have stars
GetStarredTasksIntent what items have stars
```

```
GetListTasksIntent what's in the {ListTitle} list
GetListTasksIntent what is in the {ListTitle} list
GetListTasksIntent what's in my {ListTitle} list
GetListTasksIntent what is in my {ListTitle} list
GetListTasksIntent what tasks are in the {ListTitle} list
GetListTasksIntent what items are in the {ListTitle} list
```

```
AddTaskToInboxIntent to add {TaskTitle}
AddTaskToInboxIntent to remind me to {TaskTitle}
```

```
AddTaskToListIntent to add {TaskTitle} to my {ListTitle} list
AddTaskToListIntent to add {TaskTitle} to the {ListTitle} list
```

```
AboutIntent who wrote this app
AboutIntent who wrote this skill
AboutIntent who developed this app
AboutIntent who developed this skill
AboutIntent who built this app
AboutIntent who built this skill
AboutIntent who wrote this app
```